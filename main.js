let theClock;
let theStupidPart = Math.floor(Math.random() * 30);
let stupidSeconds = Math.floor(Math.random() * 30);
let stupidMinutes = Math.floor(Math.random() * 30);

let stupidModeSecondsOn = false;
let stupidModeMinutesOn = false;
let stupidModeHoursOn = false;

let displaySec = 59;
let displayMin = 59;
let displayHou = 23;

let previousMin; 
let previousHou;

const secondsScrollBackTime = 2;

document.addEventListener("DOMContentLoaded", function(event) { 
    theClock = document.getElementById("the_clock");
    theClockHours = document.getElementById("hours");
    theClockMinutes = document.getElementById("minutes");
    theClockSeconds = document.getElementById("seconds");

    displayCurrentTime();

    setInterval( function() {
        displayCurrentTime();
    }, 1000)

});

function displayCurrentTime() {
    let today = new Date(); 
    let sec = today.getUTCSeconds();
    let hou = today.getHours();
    let min = today.getMinutes();

    // if (hours == 23 && min == 59) {
    //     theStupidPart = Math.floor(Math.random() * 30);
    // }


    // ---------------- Hours stupidification
    if (hou == 23) {
        stupidMinutes = Math.floor(Math.random() * 30);
    }

    if (hou == 0) {
        stupidModeHoursOn = true;
    }
    if (hou == stupidMinutes) {
        stupidModeHoursOn = false; 
    }

    if (stupidModeHoursOn) {
        displayHou++;
    } else {
        displayHou = hou;
    }

    
    // ---------------- Minutes stupidification
    if (min == 59) {
        stupidMinutes = Math.floor(Math.random() * 30);
        previousHou = hou;
    }

    if (min == 0) {
        stupidModeMinutesOn = true;
    }
    if (min == stupidMinutes) {
        stupidModeMinutesOn = false; 
    }

    if (stupidModeMinutesOn) {
        displayMin++;
        displayHou = previousHou;
    } else {
        displayMin = min;
    }


    // ---------------- Seconds stupidification
    if (sec == 59) {
        stupidSeconds = Math.floor(Math.random() * 30);
        previousMin = min;
    }

    if (sec == 0) {
        stupidModeSecondsOn = true;
    }
    if (sec == stupidSeconds + secondsScrollBackTime) {
        stupidModeSecondsOn = false; 
    }
    
    let int = undefined;
    if (stupidModeSecondsOn) {
        
        if (sec < stupidSeconds + secondsScrollBackTime &&
            sec > stupidSeconds) {
                
                const secondsToScrollBack = displaySec - (stupidSeconds + secondsScrollBackTime);

                const interval = 1000 / secondsToScrollBack;
                
                let i = 0;
                
                int = setInterval( function() {
                    
                    displaySec--;
                    console.log(displaySec);
                    theClockSeconds.innerHTML = ('0' + displaySec).slice(-2);

                    i++;

                    if (i == secondsToScrollBack) {
                        clearInterval(int);
                    }
                }, interval);

        } else {
            displaySec++;
        }
        displayMin = previousMin;

    } else {
        displaySec = sec;
    }

    let formSec = ('0' + displaySec).slice(-2);
    let formMin = ('0' + displayMin).slice(-2);
    let formHou = ('0' + displayHou).slice(-2);

    //theClock.innerHTML = formHou + ":" + formMin + ":" + formSec;
    theClockHours.innerHTML  = formHou;
    theClockMinutes.innerHTML = formMin;
    theClockSeconds.innerHTML = formSec;
    //theClock.innerHTML = theStupidPart;
}